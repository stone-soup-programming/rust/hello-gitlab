
struct HelloStruct {
    first: String,
    second: String,
}

impl HelloStruct {
    fn new() -> HelloStruct {
        HelloStruct {
            first: "Rust".to_string(),
            second: "Gitlab".to_string()
        }
    }
}
fn main() {
    let hello_struct = HelloStruct::new();
    println!("Hello, {} and {}!", hello_struct.first, hello_struct.second);
}

#[cfg(test)]
mod test {
    use crate::HelloStruct;

    #[test]
    fn tst() {
        let hello_struct = HelloStruct::new();
        assert_eq!(hello_struct.first, "Rust");
        assert_eq!(hello_struct.second, "Gitlab")
    }
}